package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		
		Scanner lector;
		int numeroLeido;
		double resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		for(int i = numeroLeido; i > 0 ; i--){
			resultadoDivision = (double) (numeroLeido) / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		
		lector.close();
	}

}
