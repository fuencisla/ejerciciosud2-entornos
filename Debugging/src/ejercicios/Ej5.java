package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();		

		for (int i = 1; i <= numeroLeido; i++) {			
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}	
		
		if(cantidadDivisores > 2){			
			System.out.println("No lo es");
		}else{			
			System.out.println("Si lo es");
		}
		
		
		lector.close();
	}

}
